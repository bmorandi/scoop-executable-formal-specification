This repository contains the Maude source code and the examples for the SCOOP executable formal specification. More information is available at:

Benjamin Morandi, Mischael Schill, Sebastian Nanz, and Bertrand Meyer: Prototyping a concurrency model. International Conference on Application of Concurrency to System Design, 2013, to appear